### mongodb

It is and has
	- Document Database
	- NoSQL DB
	- DeNormalized documents

- Stores data in documents. (JSON documents)


## show all databases
```
show dbs
```

## select Database
```
use <db_name>
```

## Drop a database
```
db.dropDatabase()
```

## See Current Database
```
db
```

## Create a collection
```
db.createCollection(<collection_name>
```

## show collection 
```
show collections
```

## Inserting data into a collection
```
db.<collection_name>.insertOne({'key':'value'});

e.g. - db.posts.insertOne({'title':'Hello world' , 'body':'My first post'});

give a unique id to your data -

db.posts.insertOne({_id: "1122334455", 'title':'Hello world' , 'body':'My second post'})

Multiple Object Inserting -

db.<collection_name>.insertMany(<copy the contents of the array and paste>);

db.posts.insertMany(
    [
	{
		"title":"Third Post",
		"body":"My Third Post"
	},
	{
		"title":"Fourth Post",
		"body":"My Fourth Post"
	},
	{
		"title":"Fifth Post",
		"body":"My fifth Post"
	},
	{
		"title":"Sixth Post",
		"body":"My sixth Post"
	},
	{
		"title":"seventh Post",
		"body":"My seventh Post"
	},
	{
		"title":"eighth Post",
		"body":"My Eighth Post"
	}
])
```

## Array inside array inserting
```
db.<collection_name>.insertOne(
    <Array that you want to insert>
)

e.g., - 

db.posts.insertOne(
	{
	"title":"Third Post",
	"body":"My Third Post",
	"author":{
			"name":"Vicky",
			"age":"34",
			"location":{
					"city":"BBSR",
					"zip":"454545"
				}
		},
		"tag":["Java", "OOPS"]
	})

```

## Traverse a collection 
```
db.<collection_name>.find() - print the data

db.<collection_name>.find().pretty() - print in a beautiful format

```

## Find a element
```
db.<collection_name>.find({'_id':'id_number'}})

e.g,

db.posts.find({'_id':'1122334455'})
db.posts.find({'_id':'1122334455'}).pretty()

```


